!  * Author: Martin Giuliano
!  *
!  * This program is free software: you can redistribute it and/or modify
!  * it under the terms of the GNU General Public License as published by
!  * the Free Software Foundation, either version 3 of the License, or
!  * (at your option) any later version.
!  *
!  * This program is distributed in the hope that it will be useful,
!  * but WITHOUT ANY WARRANTY; without even the implied warranty of
!  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  * GNU General Public License for more details.
!  * You should have received a copy of the GNU General Public License
!  * along with this program.  If not, see <http://www.gnu.org/licenses/>.

program jump
    implicit none
    double precision ::r,tMT,tGL,BF,m0,mi,md,gi,gd,pm,pg,pMT,pGL,g0
    double precision ::JMEANmet,JMEANgl,MUMEANmet,MUMEANgl,DISPMEANmet,DISPMEANgl,VMEANmet,VMEANgl,DELTAMUMEAN,NMEAN
    double precision , allocatable,dimension(:)::occupationmMT,occupationmGL,muMT,muGL,velMT,velGL,velmMT,velmGL,DMm,DGm,deltaMU
    double precision , allocatable,dimension(:)::dispersionMT,dispersionGL,occupationMT2,occupationGL2,Jmet,Jm,Jg,Jgl,Jmtm,Jgm
    double precision , allocatable,dimension(:)::disperMEANmet,disperMEANgl
    integer::i,l,x,k,h,p,z,n,A,jumpmet,jumpgl,side,probMET,probGL,Rep
    INTEGER:: see(20),hour
    integer, allocatable,dimension(:)::m,g,right,left,DM,DG,occupationGL,occupationMT
    character(len=15)::filename_tot
    call seed(hour,see)

    ! ENTER PARAMETER VALUES
    print*,'Enter Number of realizations:'
    read*,Rep
    print*,'Enter Number of sites:'
    read*,A
    print*,'Enter Initial amount of particles:'
    read*,z
    print*,'Enter Force strength (bF):'
    read*,BF
    print*,'Enter Number of jumps:'
    read*,n

    ! ALLOCATE PARAMETER VALUES
    allocate(occupationMT(A))
    allocate(occupationmMT(A))
    allocate(occupationGL(A))
    allocate(occupationmGL(A))
    allocate(right(A))
    allocate(left(A))
    allocate(dispersionMT(A))
    allocate(dispersionGL(A))
    allocate(disperMEANmet(A))
    allocate(disperMEANgl(A))
    allocate(occupationMT2(A))
    allocate(occupationGL2(A))
    allocate(Jmet(A))
    allocate(Jm(A))
    allocate(Jgl(A))
    allocate(Jg(A))
    allocate(Jmtm(A))
    allocate(Jgm(A))

    ! OPEN FILES
    open(1,file="mean_values.dat")
    write(1,*)'#NMEAN JMEANmet JMEANgl VMEANmet VMEANgl DELTAMUMEAN DISPMEANmet DISPMEANgl MUMEANmet MUMEANgl'
    open(2,file="dispersion_mean.dat")
    write(2,*)'#disperMEANmet disperMEANgl'
    open(unit=3,file="dispersion.dat",status='unknown')
    write(3,*)'#k dispersionMT dispersionGL'
    open(4,file="info_run.txt")

    do h=1,Rep
        z=z+2
        allocate(m(z))
        allocate(g(z))
        allocate(DM(z))
        allocate(DG(z))
        allocate(velMT(z))
        allocate(velmMT(z))
        allocate(velGL(z))
        allocate(velmGL(z))
        allocate(muMT(z))
        allocate(muGL(z))
        allocate(DMm(z))
        allocate(DGm(z))
        allocate(deltaMU(z))

        disperMEANmet=0
        disperMEANgl=0

        call seed(hour,see)
        call nume_fisier(h,filename_tot)

        occupationmMT=0
        occupationmGL=0
        occupationMT2=0
        occupationGL2=0
        velmMT=0
        velmGL=0
        DMm=0
        DGm=0
        Jm=0
        Jg=0

        occupationMT=0
        occupationGL=0
        DM=0
        DG=0
        tMT=0
        Jmtm=0
        Jgm=0

        ! PARTICLE DISPLACEMENT (VECTOR)
        do i=1,A
            right(i)=i+1
            left(i)=i-1
            right(A)=1
            left(1)=A
        enddo

        ! PARTICLES ARE DISTRIBUTED RANDOMLY
        do k=1,z
            call random_number(r)
            i=r*(A)+1
            m(k)=i
            g(k)=i
            occupationMT(i)=occupationMT(i)+1
            occupationGL(i)=occupationGL(i)+1
        enddo

        ! LOOP OF JUMPS
        do i=1,n
            ! A PARTICLE IS CHOSEN RANDOMLY
            call random_number(r)
            p=r*z+1

            ! PARTICLE p IS IN SITE l FOR METROPOLIS AND IN SITE k FOR GLAUBER
            l=m(p)
            k=g(p)

            ! CALCULATE OCCUPATION OF NEIGHBOURING SITES
            m0=occupationMT(l)
            mi=occupationMT(left(l))
            md=occupationMT(right(l))
            g0=occupationGL(k)
            gi=occupationGL(left(k))
            gd=occupationGL(right(k))

            ! METROPOLIS PROBABILITY

            call DI(side)

            if(side==1)then
                pm=pMT(side,m0,mi,md,BF)
                jumpmet=probMET(pm)
                if(jumpmet==1)then
                    m(p)=left(l)
                    x=m(p)
                    occupationMT(l)=occupationMT(l)-1
                    occupationMT(x)=occupationMT(x)+1
                    DM(p)=DM(p)-1
                    Jm(l)=Jm(l)-1
                endif
            elseif(side==2)then
                pm=pMT(side,m0,mi,md,BF)
                jumpmet=probMET(pm)
                if(jumpmet==1)then
                    m(p)=right(l)
                    x=m(p)
                    occupationMT(l)=occupationMT(l)-1
                    occupationMT(x)=occupationMT(x)+1
                    DM(p)=DM(p)+1
                    Jm(l)=Jm(l)+1
                endif
            endif

            ! GLAUBER PROBABILITY

            call DI(side)

            if(side==1)then
                pg=pGL(side,g0,gi,gd,BF)
                jumpgl=probGL(pg)
                if(jumpgl==1)then
                    g(p)=left(k)
                    x=g(p)
                    occupationGL(k)=occupationGL(k)-1
                    occupationGL(x)=occupationGL(x)+1
                    DG(p)=DG(p)-1
                    Jg(k)=Jg(k)-1
                endif
            elseif(side==2)then
                pg=pGL(side,g0,gi,gd,BF)
                jumpgl=probGL(pg)
                if(jumpgl==1)then
                    g(p)=right(k)
                    x=g(p)
                    occupationGL(k)=occupationGL(k)-1
                    occupationGL(x)=occupationGL(x)+1
                    DG(p)=DG(p)+1
                    Jg(k)=Jg(k)+1
                endif
            endif

            tMT=tMT+1./z
            tGL=tMT/2

            velMT=DM/tMT
            velGL=DG/tGL
            velmMT=velmMT+velMT
            velmGL=velmGL+velGL
            occupationmMT=occupationmMT+occupationMT
            occupationmGL=occupationmGL+occupationGL
            occupationMT2=occupationMT2+occupationMT**2
            occupationGL2=occupationGL2+occupationGL**2
            Jmtm=  Jmtm + Jm /tMT
            Jgm=Jgm+Jg/tGL
        enddo !LOOP OF JUMPS

        velmMT=velmMT/n
        velmGL=velmGL/n
        occupationmMT=occupationmMT/n
        occupationmGL=occupationmGL/n
        occupationMT2=occupationMT2/n
        occupationGL2=occupationGL2/n
        dispersionMT=(occupationMT2-occupationmMT**2)/occupationmMT**2
        dispersionGL=(occupationGL2-occupationmGL**2)/occupationmGL**2
        muMT= velmMT/BF
        muGL= velmGL/BF
        deltaMU=muGL-muMT

        Jmet=Jmtm/n
        Jgl=Jgm/n

        ! CALCULATE MEAN VALUES
        JMEANmet=0
        JMEANgl=0
        DISPMEANmet=0
        DISPMEANgl=0
        MUMEANmet=0
        MUMEANgl=0
        VMEANmet=0
        VMEANgl=0
        DELTAMUMEAN=0

        do k=1,A
            JMEANmet= JMEANmet+Jmet(k)
            JMEANgl=JMEANgl+Jgl(k)
            DISPMEANmet=DISPMEANmet+dispersionMT(k)
            DISPMEANgl=DISPMEANgl+dispersionGL(k)
        enddo

        JMEANmet=JMEANmet/A
        JMEANgl=JMEANgl/A
        DISPMEANmet=DISPMEANmet/A
        DISPMEANgl=DISPMEANgl/A

        do k=1,z
            MUMEANmet=MUMEANmet+muMT(k)
            MUMEANgl=MUMEANgl+muGL(k)
            VMEANmet=VMEANmet+velmMT(k)
            VMEANgl=VMEANgl+velmGL(k)
            DELTAMUMEAN=DELTAMUMEAN+deltaMU(k)
        enddo

        MUMEANmet=MUMEANmet/z
        MUMEANgl=MUMEANgl/z
        VMEANmet=VMEANmet/z
        VMEANgl=VMEANgl/z
        DELTAMUMEAN=DELTAMUMEAN/z

        disperMEANmet=disperMEANmet + dispersionMT
        disperMEANgl=disperMEANgl + dispersionGL

        ! FILES TO SAVE INFORMATION

        NMEAN=1.0*z/A

        write(1,*)NMEAN,JMEANmet,JMEANgl,VMEANmet,VMEANgl,DELTAMUMEAN,DISPMEANmet,DISPMEANgl,MUMEANmet,MUMEANgl

        do k=1,A
            write(3,*)k,dispersionMT(k),dispersionGL(k)
        enddo

        write(4,*)'Realization:',h
        write(4,*)'Metropolis time tMT:',tMT
        write(4,*)'Glauber time tGL:',tGL
        write(4,*)'Number of particles:',z
        write(4,*)'Number of sites:',A
        write(4,*)'Number of jumps:',n
        write(4,*)'Force:',BF

        print*,"Realization:",h

        deallocate(m)
        deallocate(g)
        deallocate(DM)
        deallocate(DG)
        deallocate(velMT)
        deallocate(velmMT)
        deallocate(velGL)
        deallocate(velmGL)
        deallocate(muMT)
        deallocate(muGL)
        deallocate(DMm)
        deallocate(DGm)
        deallocate(deltaMU)
    enddo

    disperMEANmet=disperMEANmet/Rep
    disperMEANgl=disperMEANgl/Rep

    do k=1,A
        write(2,*)k,disperMEANmet(k),disperMEANgl(k)
    enddo

    close(1)
    close(2)
    close(3)
    close(4)

endprogram jump


! SUBROUTINES:

subroutine DI(side)
    implicit none
    double precision::q
    integer::side
    call random_number(q)
    if(q<0.5)then
        side=1
    elseif(q>0.5)then
        side=2
    endif
end subroutine DI
!--------------------------------------------------
function probMET(pm)
    implicit none
    double precision::q,pm
    integer::probMET

    call random_number(q)
    if(q<=pm)then
       probMET=1
    else
       probMET=2
    endif
end function probMET
!...........................................
function probGL(pg)
    implicit none
    double precision::q,pg
    integer::probGL

    call random_number(q)
        if(q<=pg)then
        probGL=1
        else
        probGL=2
        endif
end function probGL
!---------------------------------------
function pMT(side,m0,mi,md,BF)
    implicit none
    double precision::m0,mi,md,BF,pMT,o
    integer::side
    if(side==1)then
        o=(mi+1)/m0
        pMT=o*exp(-BF)
    elseif(side==2)then
        o=(md+1)/m0
        pMT=o*exp(BF)
    endif
end function pMT
!--------------------------------------
function pGL(side,g0,gi,gd,BF)
    implicit none
    double precision::gi,gd,BF,pGL,DE,o,g0
    integer::side
    if(side==1)then
        o=g0/(gi+1)
        DE=o*exp(BF)
    elseif(side==2)then
        o=g0/(gd+1)
        DE=o*exp(-BF)
    endif
    pGL=1/(1+DE)
end function pGL
!----------------------------------------
subroutine nume_fisier (i,filename_tot)
    implicit none
    integer :: i

    integer :: integer_zeci,rest_zeci,integer_sute,rest_sute,integer_mii,rest_mii
    character(1) :: filename1,filename2,filename3,filename4
    character(4) :: filename_tot

    if(i<=9) then
        filename1=char(48+0)
        filename2=char(48+0)
        filename3=char(48+0)
        filename4=char(48+i)
    elseif(i>=10.and.i<=99) then
        integer_zeci=int(i/10)
        rest_zeci=mod(i,10)
        filename1=char(48+0)
        filename2=char(48+0)
        filename3=char(48+integer_zeci)
        filename4=char(48+rest_zeci)
    elseif(i>=100.and.i<=999) then
        integer_sute=int(i/100)
        rest_sute=mod(i,100)
        integer_zeci=int(rest_sute/10)
        rest_zeci=mod(rest_sute,10)
        filename1=char(48+0)
        filename2=char(48+integer_sute)
        filename3=char(48+integer_zeci)
        filename4=char(48+rest_zeci)
    elseif(i>=1000.and.i<=9999) then
        integer_mii=int(i/1000)
        rest_mii=mod(i,1000)
        integer_sute=int(rest_mii/100)
        rest_sute=mod(rest_mii,100)
        integer_zeci=int(rest_sute/10)
        rest_zeci=mod(rest_sute,10)
        filename1=char(48+integer_mii)
        filename2=char(48+integer_sute)
        filename3=char(48+integer_zeci)
        filename4=char(48+rest_zeci)
    endif
    filename_tot=''//filename1//''//filename2//''//filename3//''//filename4//''
    return
 end subroutine nume_fisier
!----------------------------------------------
subroutine seed(hour,see)
    INTEGER see(20),hour
    CALL SYSTEM_CLOCK(hour)
    see=hour
    CALL RANDOM_SEED(put=see)
    CALL RANDOM_SEED(get=see)
    return
end subroutine seed
